import geopy.distance
import vantage_api.geometry as VantageGeo
import cv2
import numpy as np
from .track import ObjectTracker
import math
  
# Latitude = y
# Longitude = x  

def video_test(input_file, geo: VantageGeo.VantageGeometry, output_file):
    video = cv2.VideoCapture(input_file)
    fps = video.get(cv2.CAP_PROP_FPS)
    frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColour = (0, 0, 255)
    fontThickness = 2
    print('fps: '+str(fps)+' width:'+str(frame_width)+' height:'+str(frame_height))
    frame_id = -1
    out = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'DIVX'), fps, (frame_width, frame_height))
    objectTracker = ObjectTracker()
    center = None
    while True:
        ret, frame = video.read()
        frame_id = frame_id + 1
        if not ret:
            break
        print('processing: '+str(frame_id))

        frame_geo = geo.getFrame(frame_id)
        frame_geo.printBoxMarkers()
        if center == None:
            center = frame_geo.getSceneCentreLongLat()

        box = frame_geo.getSceneFrameLongLat()
        #lat lng to px
        if frame_geo.isSouthernHem():
            y = box[0][1] - box[3][1]
            yc = (center[1] - box[3][1])
        else:
            y = box[3][1] - box[0][1]
            yc = (center[1] - box[0][1])
           
  
        yScew = abs(box[0][1] - box[1][1])
        xScew = box[0][0] - box[2][0]

        x = box[1][0] - box[0][0]
        xc = (box[1][0] - center[0])
       

        #work our y offset
        a = yScew
        b = x
        c = math.sqrt((a * a) + (b * b))
        aa = np.arcsin(a/c)
        yScew2 = xc * np.tan(aa)

        a = xScew
        b = y
        c = math.sqrt((a * a) + (b * b))
        aa = np.arcsin(a/c)
        xScew = yc * np.tan(aa)

        xp =  ((xc)/x ) * 100   
        yp =  ((yc + yScew2)/y ) * 100   
        print(yp, xp)
        

        ypx = frame_height * (yp /100)  
        xpx = frame_width * (xp /100)

        if frame_geo.isSouthernHem():
            ypx = frame_height - ypx

        #px to lat lng
        ixp = (xpx / frame_width) * 100
        iyp = (ypx / frame_height) * 100

        newLat = (y * (iyp / 100)) + box[3][1]
        newLng = (x * (ixp / 100)) + box[3][0]

        boxSize = 10
        box = [
            (int(xpx - (boxSize / 2)),int(ypx - (boxSize / 2))),
            (int(xpx + (boxSize / 2)),int(ypx + (boxSize / 2)))
        ]
        #objectTrackerObject = objectTracker.addObjectData(newLng,newLat, frame_id)
        #print(str(frame_id) +' : ' + frame_geo.getFrameId() +' : '+objectTrackerObject.getText())

        cv2.rectangle(frame, box[0],box[1],(255, 0, 0), 2)  
        #cv2.putText(frame, 'Moved: '+str(objectTrackerObject.getDistanceMoved()), (box[0][0] - 50,box[0][1] + 100), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
        out.write(frame)
    out.release()    







def detect_video(do_prediction, input_file, geo: VantageGeo.VantageGeometry, score_filter=0.93, verbose= False, output_file = None):
    video = cv2.VideoCapture(input_file)
    fps = video.get(cv2.CAP_PROP_FPS)
    frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    if verbose:
        print('fps: '+str(fps)+' width:'+str(frame_width)+' height:'+str(frame_height))
        print('copy plots to here: https://mobisoftinfotech.com/tools/plot-multiple-points-on-map/')
    frame_id = -1

    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColour = (0, 0, 255)
    fontThickness = 2
    markerColours = ['red','blue','green']
    first_frame = None

    objectTracker = ObjectTracker()
    if output_file != None:
        out = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'DIVX'), fps, (frame_width, frame_height))
    while True:
        ret, frame = video.read()
        frame_id = frame_id + 1
        if not ret or frame_id >2:
            break

        frame_geo = geo.getFrame(frame_id)
        if first_frame == None:
            first_frame = frame_geo

        if verbose == True and output_file != None:
            print('')
            print('frame: '+str(frame_id))
            frameBounds = frame_geo.getSceneFrameLongLat()
            frameCenter = frame_geo.getSceneCentreLongLat()
            satCenter = frame_geo.getSatelliteLongLat()

            cv2.putText(frame, 'Frame ID: '+frame_geo.getFrameId() + ', ' + str(frame_id), (50, 50), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Frame Center: '+str(frameCenter[0]) + ', '+ str(frameCenter[1]), (50, 100), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Sat Center: '+str(satCenter[0]) + ', '+ str(satCenter[1]), (50, 150), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Top Left: '+str(frameBounds[0][0]) + ', '+ str(frameBounds[0][1]), (50,200), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Top Right: '+str(frameBounds[1][0]) + ', '+ str(frameBounds[1][1]), (50,250), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Bottom Right: '+str(frameBounds[2][0]) + ', '+ str(frameBounds[2][1]), (50,300), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
            cv2.putText(frame, 'Bottom Left: '+str(frameBounds[3][0]) + ', '+ str(frameBounds[3][1]), (50,350), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
                            
            centerCheck = first_frame.pointToLongLat(frame_width, frame_height, frame_width/2,frame_height/2)
            cv2.rectangle(frame, (int(frame_width/2 - 10),int(frame_height/2 - 10)),(int(frame_width/2 + 10),int(frame_height/2 + 10)), (0, 255, 0), 2)
            

        for prediction in do_prediction(frame):
            if(prediction.getScore() < score_filter):
                continue
            center = prediction.getCenter()
            box = prediction.getBox()
            pos = frame_geo.pointToLongLat(frame_width, frame_height, center[0],center[1])
            
            objectTrackerObject = objectTracker.addObjectData(pos[0], pos[1], frame_id)
            if verbose == True:
                print('x', center[0] / frame_width,'y', center[1] / frame_height)
                print(str(frame_id) +' : ' + frame_geo.getFrameId() +' : '+objectTrackerObject.getText())
                print(str(frameBounds[0][1])+','+str(frameBounds[0][0])+','+markerColours[0]+',marker')
                print(str(frameBounds[1][1])+','+str(frameBounds[1][0])+','+markerColours[0]+',marker')
                print(str(frameBounds[2][1])+','+str(frameBounds[2][0])+','+markerColours[0]+',marker')
                print(str(frameBounds[3][1])+','+str(frameBounds[3][0])+','+markerColours[0]+',marker')
                print(str(centerCheck[1])+','+str(centerCheck[0])+',yellow,marker') 
                #print(str(frameCenter[1])+','+str(frameCenter[0])+',green,marker') 
                print(str(pos[1])+','+str(pos[0])+','+markerColours[1]+',marker') 
                 

            if output_file != None:
                #write pretection
                cv2.rectangle(frame, (box[0],box[1]),(box[2],box[3]), (255, 0, 0), 2) 
                cv2.putText(frame, 'ID: '+str(objectTrackerObject.getLabel()), (box[2] - 50,box[3] + 50), font, fontScale, fontColour, fontThickness, cv2.LINE_AA) 
                cv2.putText(frame, 'Moved: '+str(objectTrackerObject.getDistanceMoved()), (box[2] - 50,box[3] + 100), font, fontScale, fontColour, fontThickness, cv2.LINE_AA)
                #cv2.putText(frame, 'x: '+str(center[0])+', y:'+str(center[1]), (box[2] - 50,box[3] + 150), font, fontScale, fontColour, fontThickness, cv2.LINE_AA) 
        if output_file != None:
            if frame_id <= 2:
                cv2.imwrite('output/frame_'+str(frame_id)+'.jpg',frame)
            out.write(frame)

    video.release()
    if output_file != None:
        out.release()

    return objectTracker    


