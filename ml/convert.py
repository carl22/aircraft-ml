from email.mime import base
import cv2
import csv
import json
import re
import glob
import os.path as path
from PIL import Image, ImageOps

def singlise(datasetPath = './dataset/all/', annotationCsv = './dataset/all/annotations.csv', outputPath='./dataset/singles/', outputAnnotationCsv= './dataset/singles/annotations.csv', pad=False):
    maxWidth = 0
    maxHeight = 0
    with open(annotationCsv, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rowidx = 0
        for row in spamreader:
            rowidx = rowidx + 1
            if rowidx > 1:
                width = int(row[6]) - int(row[4])
                height = int(row[7]) - int(row[5])
                if width > maxWidth:
                    maxWidth = width
                if height > maxHeight:
                    maxHeight = height    

    print(maxWidth, maxHeight)

    with open(outputAnnotationCsv,'w', newline='') as csvfile:  
        csvWriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvWriter.writerow(['filename','width','height','class','xmin','ymin','xmax','ymax','image_id'])  

        with open(annotationCsv, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            rowidx = 0
            for row in spamreader:
                rowidx = rowidx + 1
                if rowidx == 1:
                    continue
                outputFilename = str(rowidx) + '_' + row[0]
                print('processing: '+outputFilename)
                img = Image.open(datasetPath+row[0])
                    
                width = int(row[6]) - int(row[4])
                height = int(row[7]) - int(row[5])
                
                if pad:
                    #area = ImageOps.pad(area, (maxWidth, maxHeight), method = 0, centering =(0.5, 0.5))
                    xc = int(row[6]) - ((int(row[6]) - int(row[4])) / 2)
                    yc = int(row[7]) - ((int(row[7]) - int(row[5])) / 2)
                    xOffset = (maxWidth) / 2
                    yOffset = (maxHeight) / 2
                    x = xc - xOffset
                    x2 = xc + xOffset
                    y = yc - yOffset
                    y2 = yc + yOffset
                    area = img.crop((x,y,x2,y2))

                    width = maxWidth
                    height = maxHeight
                else:
                    area = img.crop((int(row[4]),int(row[5]),int(row[6]),int(row[7])))   
                area.save(outputPath+outputFilename)    

                csvWriter.writerow([outputFilename,width,height,row[3],0,0,width,height,rowidx - 2])      


def testCsv(datasetPath = './dataset/hackathon/dataset/', annotationCsv = './dataset/hackathon/annotations.csv', outputPath='./dataset/hackathon/test/'):
    with open(annotationCsv, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        lastImageId = -1
        rowidx = 0
        opened = False
        for row in spamreader:
            rowidx = rowidx + 1
            if rowidx <= 1: 
                continue
            filename, w,h,label,x,y,x2,y2, image_id = row

            if(opened == True and lastImageId != image_id):
                print('writing: '+filename)
                cv2.imwrite(outputPath + filename, im)
                opened = False
           
            if lastImageId != image_id:
                print('processing: '+filename)
                im = cv2.imread(datasetPath + filename)
                opened = True
            print('     rect : '+x+','+y+','+x2+','+y2)    
            cv2.rectangle(im, (int(float(x)),int(float(y))),(int(float(x2)),int(float(y2))), (255, 0, 0), 2) 
            

            lastImageId = image_id  

        cv2.imwrite(outputPath + filename, im)    


def trim(str):
    return str.strip()

def cvat2csv(
    datasetPath = './dataset/hackathon/dataset/', 
    classNameFile = './dataset/hackathon/classes.names', 
    saveCsvPath = './dataset/hackathon/annotations.csv',
    largeOnly = False,
    smallOnly = False,
    widthBreakpoint = 100
    ):
    print('finding: '+datasetPath+'*.jpg')
    with open(classNameFile) as f:
        classes = list(map(trim,f.readlines()))

    with open(saveCsvPath,'w', newline='') as csvfile:  
        csvWriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvWriter.writerow(['filename','width','height','class','xmin','ymin','xmax','ymax','image_id'])  

        files = glob.glob(datasetPath+'*.jpg')
        image_id = -1
        for file in files:
            im = cv2.imread(file)
            h, w, c = im.shape
            if largeOnly and w < widthBreakpoint:
                continue
            if smallOnly and w > widthBreakpoint:
                continue
            image_id = image_id + 1
            print('processing: '+file)
            filePath, extension = path.splitext(file)
            basename = path.basename(filePath)
            if path.exists(datasetPath + basename + '.txt'):
                with open(datasetPath + basename + '.txt') as f:
                    lines = list(map(trim,f.readlines()))
                    for line in lines:
                        labelIdx, cx, cy, aw, ah = line.split(' ')
                        label = classes[int(labelIdx)]
                        xMin = w * float(cx) - ((float(aw) * w) /2 )
                        yMin = h * float(cy) - ((float(ah) * h) /2 )
                        xMax = xMin + (float(aw) * w)
                        yMax = yMin + (float(ah) * h)
                        data = [basename + '.jpg', w, h, label, xMin, yMin, xMax, yMax,image_id]
                        csvWriter.writerow(data)
    print('done')                    

def via2csv():
    f = open('dataset/validation/validationset.json')
    data = json.load(f)
    with open('dataset/validation/annotations.csv','w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['filename','width','height','class','xmin','ymin','xmax','ymax','image_id'])
        img_id = -1
        for id in data['_via_img_metadata']:
            img_id = img_id + 1
            meta = data['_via_img_metadata'][id]
            filename = meta['filename']
            img = cv2.imread('dataset/validation/' + filename)
            width = img.shape[1]
            height = img.shape[0]
            for region in meta['regions']:
                print(region)
                shape = region['shape_attributes']
                print(shape)
                spamwriter.writerow([filename,width,height,'Airplane',shape['x'],shape['y'],shape['x'] + shape['height'],shape['y'] + shape['width'],img_id])
                #cv2.rectangle(img, (shape['x'], shape['y']), (shape['x'] + shape['height'], shape['y'] + shape['width']), color = (0, 0, 255), thickness = 3)
            #cv2.imwrite('dataset/planes/test/' + filename, img)    


    print('csv written')            

# Only used to convert orignal annotations file.
def convertCSV():
    images = []
    with open('dataset/annotations.csv', newline='') as csvfile:
        with open('dataset/annotations2.csv', 'w', newline='') as csvwfile:
            spamwriter = csv.writer(csvwfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(['filename','width','height','class','xmin','ymin','xmax','ymax','image_id'])
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in spamreader:
                xmin = -1
                ymin = -1
                xmax = 0
                ymax = 0
                m = re.findall('[0-9]+', row[2])
                for idx, c in enumerate(m):
                    if idx % 2 == 1:
                        if(ymin == -1 or int(c) < ymin):
                            ymin = int(c)
                        if(int(c) > ymax):
                            ymax = int(c)    
                    else:
                        if(xmin == -1 or int(c) < xmin):
                            xmin = int(c)
                        if(int(c) > xmax):
                            xmax = int(c) 
                if(row[3] == 'Airplane'):
                    if(row[1] not in images):
                        images.append(row[1])
                    spamwriter.writerow([row[1],'2560','2560',row[3],xmin,ymin,xmax,ymax,images.index(row[1])])  

