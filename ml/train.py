from detecto.core import Model, Dataset, DataLoader
from detecto.visualize import detect_video, show_labeled_image,plot_prediction_grid
from detecto import utils
from torchvision import transforms
import numpy as np
import cv2

def trainModel(annotations, imagesFolder, pthFile, epochs = 5, val_dataset = None, learning_rate=0.005, momentum=0.9,
            weight_decay=0.0005, gamma=0.1, lr_step_size=3, batch_size=1, labels=['Airplane','Truncated_airplane'],transform=None):
    dataset = Dataset(annotations, imagesFolder,transform=transform)
    print(len(dataset))
    
    loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    model = Model(labels)
    if val_dataset != None:
        print('Training using validation set')
        model.fit(loader,val_dataset, epochs=epochs,learning_rate=learning_rate, verbose=True, momentum=momentum, weight_decay= weight_decay, gamma= gamma, lr_step_size= lr_step_size)
    else:
        print('Training without validation set')
        model.fit(loader, epochs=epochs,learning_rate=learning_rate, verbose=True, momentum=momentum, weight_decay= weight_decay, gamma= gamma, lr_step_size= lr_step_size)
    model.save(pthFile)
    return model

def loadModel(pthFile, labels=['Airplane','Truncated_airplane']): 
    return Model.load(pthFile, labels)    

def predict(model, testImage):
    image = utils.read_image(testImage) 
    labels, boxes, scores = model.predict(image)  # Get all predictions on an image
    predictions = model.predict(image)  # Same as above, but returns only the top predictions
    return predictions
    

def detect_image(imagePath, outputImage, predictions):
    image = cv2.imread(imagePath)
    for bbox in predictions[1]:
        bound = bbox.numpy()
        cv2.rectangle(image, (int(bound[0]), int(bound[1])), (int(bound[2]), int(bound[3])), color = (0, 0, 255), thickness = 3)

    cv2.imwrite(outputImage, image)

def detect_video(model, input_file, output_file, fps=30, score_filter=0.6):
     # Read in the video
    video = cv2.VideoCapture(input_file)

    # Video frame dimensions
    frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Scale down frames when passing into model for faster speeds
    scaled_size = 800
    scale_down_factor = min(frame_height, frame_width) / scaled_size

    # The VideoWriter with which we'll write our video with the boxes and labels
    # Parameters: filename, fourcc, fps, frame_size
    out = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'DIVX'), fps, (frame_width, frame_height))

    # Transform to apply on individual frames of the video
    transform_frame = transforms.Compose([  # TODO Issue #16
        transforms.ToPILImage(),
        transforms.Resize(scaled_size),
        transforms.ToTensor(),
        utils.normalize_transform(),
    ])

    # Loop through every frame of the video
    while True:
        ret, frame = video.read()
        # Stop the loop when we're done with the video
        if not ret:
            break

        # The transformed frame is what we'll feed into our model
        # transformed_frame = transform_frame(frame)
        transformed_frame = frame  # TODO: Issue #16
        predictions = model.predict(transformed_frame)

        # Add the top prediction of each class to the frame
        for label, box, score in zip(*predictions):
            if score < score_filter:
                continue


            bound = box.numpy()

            # Since the predictions are for scaled down frames,
            # we need to increase the box dimensions
            # box *= scale_down_factor  # TODO Issue #16

            # Create the box around each object detected
            # Parameters: frame, (start_x, start_y), (end_x, end_y), (r, g, b), thickness
            cv2.rectangle(frame, (int(bound[0]), int(bound[1])), (int(bound[2]), int(bound[3])), (255, 0, 0), 3)

            # Write the label and score for the boxes
            # Parameters: frame, text, (start_x, start_y), font, font scale, (r, g, b), thickness
            #cv2.putText(frame, '{}: {}'.format(label, round(score.item(), 2)), (bound[0], bound[1] - 10),
            #            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)

        # Write this frame to our video file
        out.write(frame)

        # If the 'q' key is pressed, break from the loop
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break

    # When finished, release the video capture and writer objects
    video.release()
    out.release()

    # Close all the frames
    cv2.destroyAllWindows()