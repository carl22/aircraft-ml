import geopy.distance

DISTANCE_DEVIATION = 3  

class ObjectTrackerObject:
    currentLat = 0
    currentLong = 0
    frame_id = 0
    positions = []
    distanceDevitation = 0 #meters
    label = None

    def __init__(self, long, lat, label,frame_id, distanceDevitation = DISTANCE_DEVIATION):
        self.label = label
        self.distanceDevitation = distanceDevitation
        self.positions = []
        self.addPosition(long, lat, frame_id)

    def getLabel(self):
        return self.label    

    def getCurrentFrameId(self):
        return self.frame_id

    def setDistanceDevation(self, devation):
        self.distanceDevitation = devation
        return self       

    def addPosition(self, long, lat, frame_id):
        self.currentLat = lat
        self.currentLong = long
        self.frame_id = frame_id
        self.positions.append([long, lat, frame_id])

    def getDistanceFrom(self, long, lat):
        coords_1 = (self.currentLat, self.currentLong)
        coords_2 = (lat, long)
        return geopy.distance.geodesic(coords_1, coords_2).meters

    def isWithinDistanceOf(self, long, lat, frame_id):
        deviation = self.distanceDevitation 
        distance =  self.getDistanceFrom(long, lat)
        print('distance: '+str(distance))
        return distance <= deviation      

    def getCurrentLocation(self):
        return [self.currentLong, self.currentLat]

    def count(self):
        return len(self.positions) 

    def getLocations(self):
        return self.positions

    def getDistanceMoved(self):
        if len(self.positions) >= 2:
            idx = len(self.positions) - 2
            return self.getDistanceFrom(self.positions[idx][0], self.positions[idx][1])    
        return 0

    def getText(self):
        return 'ID: ' + str(self.label) + ' ('+str(self.currentLong)+', '+str(self.currentLat)+') Moved: '+str(self.getDistanceMoved())  

class ObjectTracker:
    objects = []
    def __init__(self):
        self.objects = []
    
    def addObjectData(self, long, lat, frame_id):
        closestObject = None
        for object in self.objects:
            if(object.isWithinDistanceOf(long, lat, frame_id)):
                if closestObject == None or object.getDistanceFrom(long, lat) < closestObject.getDistanceFrom(long, lat):
                    closestObject = object
        if(closestObject == None):
            closestObject = ObjectTrackerObject(long, lat, len(self.objects), frame_id)
            self.objects.append(closestObject)
        else:
            closestObject.addPosition(long, lat, frame_id)  

        return closestObject              

    def getObjects(self):
        return self.objects

    def getObjectByLabel(self, label):
        for object in self.objects:
            if object.getLabel() == label:
                return object
        return None   