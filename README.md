# Example ML Training for Aircraft in Airports

this is an example program utalising detecto libuary to perform traning and location of aircraft at airports using
this dataset. https://www.kaggle.com/airbusgeo/airbus-aircrafts-sample-dataset

The annotations.csv has been translated into annotation2.csv to create a csv format that can be read by detecto/pytorch

training has been done to 5 epochs and genrates airbus.pth in the root folder. This is used to test images against the trained model.

## Running the model

    docker-compose run --rm train

this will output test.png in the output folder    

## Training

Adjust the main.py file to `trainModel` rather than `loadModel` you must have the images from the link above in `dataset/images` folder
