# Use the official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.9.7-slim-buster

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y

# Copy local code to the container image.
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

# Install production dependencies.
RUN pip install --no-cache-dir -r requirements.txt

CMD exec python main.py