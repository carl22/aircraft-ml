
from array import array


class GeoPoint:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def toString(self):
        str(self.x) + ' ' + str(self.y)

class GeoRequest:
    points = []

    def fromArray(self, arrayData = []):
        for point in arrayData:
            self.append(GeoPoint(point[0], point[1]))
        return self    

    def append(self, point: GeoPoint):
        self.points.append(point)
        return self
        
    def toString(self):
        outputString = 'POLYGON(('
        idx = 0
        for point in self.points:
            if idx > 0:
                outputString = outputString + ', '
            outputString = outputString + point.toString()
            idx = idx + 1
        outputString = outputString + '))'
        return outputString        

