import os
from vantage_api.api import VantageServiceApi
import vantage_api.utils as vantage_utils
import vantage_api.constants as vantage_constants
import vantage_api.geometry as VantageGeo
import ml.train as ml
import ml.utils as mlUtils
import ml.convert as mlConvert
import ml.helpers as mlHelper
from detecto.core import Dataset
from torchvision import transforms
from detecto import utils



def process_video(videoFolder, modelPath='plane.pth', outPath='output/'):
    geoFile='dataset/vivid-x/'+videoFolder+'/'+videoFolder+'_geometry.xml'
    videoFile='dataset/vivid-x/'+videoFolder+'/'+videoFolder+'.mp4'
    outFile = outPath+videoFolder+'.mp4'
    #Load Vantage Geo Data
    geo = VantageGeo.VantageGeometry(geoFile)
    #Load Trained Model
    model = ml.loadModel(modelPath)
    #Replace handler with your own
    def prediction_handler(model):
        def process(frame):
            predictions = model.predict(frame)
            outputData = []
            for label, box, score in zip(*predictions):
                bound = box.numpy()
                outputData.append(mlHelper.Prediction(label,score, int(bound[0]), int(bound[1]), int(bound[2]), int(bound[3])))
            return outputData    

        return process    
    #Perform detection
    trackedObjects = mlUtils.detect_video(prediction_handler(model), videoFile, geo, verbose=True, output_file=outFile)
    return trackedObjects



def api_download():

    vantageApi = VantageServiceApi(os.environ.get('VANTAGE_USER'),os.environ.get('VANTAGE_KEY'))

    #services = vantageApi.getSercice(11)
    #print(services.json())

    print(vantageApi.downloadFiles(
        'https://esrin-data-input.s3.eu-west-2.amazonaws.com/Vivid-X2/VX020002b2.zip', 
        'input',
        verbose=True
    ))

    #geoData = vantage_utils.GeoRequest()
    #geoData.fromArray([
    #    [165.12764544844623, -47.53375265272916],
    #    [179.5568484551946, -47.53375265272916],
    #    [179.5568484551946, -33.63503505064067],
    #    [165.12764544844623, -33.63503505064067],
    #    [165.12764544844623, -47.53375265272916]
    #])
    #print(vantageApi.search(geoData).getFeatures())



def train_model(epoch = 10, learning_rate=0.001, momentum=0.9):
    #val dataset is optional
    val_dataset = Dataset('dataset/validation/annotations.csv', 'dataset/validation')
    custom_transforms = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize(1024),
        #transforms.ColorJitter(saturation=0.3),
        transforms.ToTensor(),
        utils.normalize_transform(),
    ])
    model = ml.trainModel(
        'dataset/vantage/annotations.csv', 
        'dataset/vantage',
        'plane.pth', 
        epoch, 
        val_dataset = val_dataset, 
        learning_rate=learning_rate, 
        momentum=momentum,
        transform=custom_transforms
    )
    return model

#converts cvat txt files into a annotations.csv file for model
#mlConvert.cvat2csv()
#mlConvert.testCsv()
#mlConvert.singlise()

#Train the model
#train_model()

#process test video with model
process_video('VX020001c0')

videoFolder = 'VX020001c0'
geoFile='dataset/vivid-x/'+videoFolder+'/'+videoFolder+'_geometry.xml'
videoFile='dataset/vivid-x/'+videoFolder+'/'+videoFolder+'.mp4'
outFile = 'output/'+videoFolder+'.mp4'
geo = VantageGeo.VantageGeometry(geoFile)


#mlUtils.video_test(videoFile, geo, outFile)

#utils.split_video(video_file, output_folder, prefix='frame', step_size=1)¶